let fs=require('fs');

let data = { 
    "max": {
        colors: ['Orange', 'Red']
    }
};

let data1 = JSON.stringify(data);

// Q1. Create a new file data.json

fs.writeFile('./data.json',data1,(err)=>{
    if(err){  console.log(err.message);  }
    else {   console.log('data writed to file');  }  
})

// Q2. Copy the contents of the data.json into a new folder output/data.json and delete the original file.

fs.readFile('./data.json',('utf-8'),(err,data)=>{
    if(err){
        console.log(err.message);  }
    else{
        fs.writeFile('./output/data.json',data,(err)=>{
            if(err){  console.log(err.message);  }
            else{  console.log("data copied"); }
        })
    }
})
setTimeout(() => {
    fs.unlink('./data.json',(err)=>{
        if(err) { console.log(err.message)}
        else( console.log("data.json file deleted"))
    })
},1000);


//Q3. Write a function that takes a person name and fav color as parameters and writes to data.json file

function addData(name,color){
fs.readFile('./output/data.json',('utf-8'),(err,data)=>{
    if(err) { console.log(err.message);  }
    else{
        let data1=JSON.parse(data);
        data1[name]={ color: color}
        fs.writeFile('./output/data.json',JSON.stringify(data1),(err)=>{
            if(err){ console.log(err.message)}
            else{ console.log("dataUpdate_1")}
        })
    }
})
}
addData("srikanth",["red","blue"]);

 
//Q4. Write a function that takes a person name and fav hobby as param and add that hobby as a separate key and write to data.json.
   // (Do not replace the old content).


function addData2(name,hobby){
    fs.readFile('./output/data.json',('utf-8'),(err,data)=>{
        if(err) { console.log(err.message);  }
        else{
            let data1=JSON.parse(data);
            if(data1[name]){ data1[name].hobby = hobby }
            else{
                data1[name] = {'hobby' : hobby };
            }
            console.log(data1);
            fs.writeFile('./output/data.json',JSON.stringify(data1),(err)=>{
                if(err){ console.log(err.message)}
                else{ console.log("dataUpdate_2")}
            })
        }
    })
    }
addData2("srikanth",["listening","eating"]);